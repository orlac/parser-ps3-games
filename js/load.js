var ajaxEvent = {
    eventPreload : 'eventPreload',
    eventLoad : 'eventLoad'
}
var AjaxLoad = function()
{
    this.preload = function()
    {
	jQuery(document).trigger(ajaxEvent.eventPreload);
    }
    
    this.onload = function()
    {
	jQuery(document).trigger(ajaxEvent.eventLoad);
    }
    
    this.query = function(options)
    {
	var onSuccess = options.success;
	var onError = options.error; 
	var preload = this.preload;
	var onload = this.onload;
	preload();
	options.data = options.data || {};
	options.data.rand = Math.random();
	options.success = function(data)
	{
	    if(typeof data.success != 'undefined' && data.success == true)
	    {
		onSuccess(data.data);
		onload();
	    }else
	    {
		if(typeof data.errors == 'undefined')
		{
		    data.errors = ['some error'];
		}
		onError(data);
		onload();
	    }
	}
	jQuery.ajax(options);
    }
}

var ajax = new AjaxLoad();