var parser4k = 
{
    sendData: {ajax: true, params: {}},
    onError: function(data, text)
    {
	console.log(data);
        alert(data.errors.join("\n"));
    },
    event: 
	{
	    load: 'loadeventparser',
	    complete: 'completeeventparser'
	},
    
    start: function()
    {
	switch(parser4k.mode)
	{
	    case 'start':
		parser4k.getAction('clear', {}, function(data)
		   {
		       parser4k.parser();
		   });
	    break;
            case 'continue':  
		parser4k.parser();
	    break;
	}
	
	
    },
    
    mode: 'start' /* continue */, 
    parsedMarks: {},
    getParsed: function(call_back)
    {
	parser4k.getAction('getParsedMarks', {}, function(data)
	{
	    var all = []//data.all;
	    var parsed = []//data.parsed;
	    for(var i in data.all)
		all.push(data.all[i])
	    for(var i in data.parsed)
		parsed.push(data.parsed[i])
	    if(all.length == parsed.length)
	    {
		//начать заново
		parser4k.mode = 'start';
		jQuery(document).trigger(parser4k.event.load, parser4k.mode);
		call_back(all, parsed);
	    }else if(parsed.length < all.length )
	    {
		//продолжить
		parser4k.mode = 'continue';
		jQuery(document).trigger(parser4k.event.load, parser4k.mode);
		var m = parsed.pop();
		parser4k.getAction('clear', {mark: m}, function(data)
		{
		    for(var i in parsed)
		    {
			var m = parsed[i];
			parser4k.parsedMarks[parsed[i]] = parsed[i];
		    }
		    call_back(all, parsed);
		});
	    }
	});
	
    },
    
    parser: function()
    {
	
	parser4k.sendData.action = 'getMarks';
	
	ajax.query(
	{
	   dataType : 'json',
	   url : '/web.php',
	   data : parser4k.sendData,
           type: 'post',
	   success: function(data, event) 
	   {
	       var ev = {}
	       ev.action = 'getMarks';
	       ev.data = data.marks;
	       ev.request = data.params;
	       jQuery(document).trigger(parser4k.event.complete, ev);
	       for (var i  in data.marks)
	       {
		   var mark = data.marks[i];
                   if(typeof parser4k.parsedMarks[mark] != 'undefined' && parser4k.mode == 'continue')
		   {
		       continue;
		   }
		   
		   data.params.mark = mark;
		   parser4k.getAction('getModels', data.params, function(data)
		   {
		       var ev = {}
		       ev.action = 'getModels';
		       ev.data = data.models;
		       ev.request = data.params;
		       jQuery(document).trigger(parser4k.event.complete, ev);
		       
		       for (var model in data.models)
		       {
			   data.params.model = model;
			   parser4k.getAction('getYears', data.params, function(data)
			   {
			       var ev = {}
			       ev.action = 'getYears';
			       ev.data = data.years;
			       ev.request = data.params;
			       jQuery(document).trigger(parser4k.event.complete, ev);
			       
			       for (var year in data.years)
			       {
				   data.params.year = year;
				   parser4k.getAction('getModification', data.params, function(data)
				   {
				       var ev = {}
				       ev.action = 'getModification';
				       ev.data = data.mods;
				       ev.request = data.params;
				       jQuery(document).trigger(parser4k.event.complete, ev);
				       
				       for (var mod in data.mods)
				       {
					   data.params.mod = mod;
					   parser4k.getAction('getSizes', data.params, function(data)
					   {
					       /*var ev = {}
					       ev.action = 'getSizes';
					       ev.data = data.sizes;
					       ev.request = data.params;
					       jQuery(document).trigger(parser4k.event.complete, ev);
					       
					       var tyres = data.sizes.tyre;
					       var disks = data.sizes.disk;
					       
					       var shtat = 0;
					       //while(tyres.length > 0)
					       for(var t in tyres)
					       {
						   //var urls = tyres.shift();
						   var urls = tyres[t];
						   while(urls.length > 0)
						   {
						       var url = urls.shift();
						       data.params.url = url;
						       data.params.shtat = shtat;
						       parser4k.getAction('getAddTyres', data.params, function(data)
						       {
							   var ev = {}
							   ev.action = 'getAddTyres';
							   ev.data = data.tyres;
							   ev.request = data.params;
							   jQuery(document).trigger(parser4k.event.complete, ev);
						       }, true);
						   }
						   shtat++;
					       }
					       var shtat = 0;
					       //while(disks.length > 0)
					       for(var d in disks)
					       {
						   //var urls = disks.shift();
						   var urls = disks[d];
						   while(urls.length > 0)
						   {
						       var url = urls.shift();
						       data.params.url = url;
						       data.params.shtat = shtat;
						       parser4k.getAction('getAddDiscs', data.params, function(data)
						       {
							   var ev = {}
							   ev.action = 'getAddDiscs';
							   ev.data = data.discs;
							   ev.request = data.params;
							   jQuery(document).trigger(parser4k.event.complete, ev);
						       }, true);
						   }
						   shtat++;
					       }*/
					       
					   }, true);
				       }
				   })
			       }
			   });
		       }
		   });
	       }
	   },
	   error: parser4k.onError
	});
    },
    
    getModels: function(marks)
    {
	var mark = marks.shift();
	
    },
    
    getAction: function(action, params, call_back, async)
    {
	async = async || false;
	parser4k.sendData.params = params;
	parser4k.sendData.action = action;
	var _data = parser4k.sendData;
	var ev = 
	    {
		action: action,
		params: _data
		
	    };
	jQuery(document).trigger(parser4k.event.load, ev);
	ajax.query(
	{
	   dataType : 'json',
	   url : '/web.php',
	   data : _data,
	   async: async,
           type: 'post',
	   success: function(data) 
	   {
	       call_back(data);
	   },
	   error: parser4k.onError
	});
        console.log(_data);
	
	/*window.setTimeout(function()
	{
	    ajax.query(
	    {
	       dataType : 'json',
	       url : '/web.php',
	       data : _data,
	       async: false,
	       success: function(data) 
	       {
		   call_back(data);
	       },
	       error: parser4k.onError
	    });
	    
	}, 2000)*/
	
	
    }
}