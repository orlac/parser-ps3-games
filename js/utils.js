var utils = 
{   
    print_r: function (theObj, str, ident){
	  var print_r = utils.print_r;
	  if(typeof theObj != 'undefined' 
	      && ( typeof  theObj == 'array' || typeof   theObj == 'object' ) ){
	    str += ("<ul>");
	    for(var p in theObj){
	      if(typeof theObj[p] != 'undefined' &&
	      ( typeof theObj[p] == 'array' || typeof   theObj[p] == 'object' ) ){
		str += ("<li>["+p+"] => "+typeof(theObj)+"</li>");
		str += ("<ul>");
		str = print_r(theObj[p], str);
		str += ("</ul>"); 
	      } else {
		str += ("<li>["+p+"] => "+theObj[p]+"</li>");
	      }
	    }
	    str += ("</ul>");
	    return str;
	  }
    },
    
    print_r_text: function (theObj, str, ident){
	  var print_r = utils.print_r_text;
	  if(typeof theObj != 'undefined' 
	      && ( typeof  theObj == 'array' || typeof   theObj == 'object' ) ){
	    str += ("\n");
	    for(var p in theObj){
	      if(typeof theObj[p] != 'undefined' &&
	      ( typeof theObj[p] == 'array' || typeof   theObj[p] == 'object' ) ){
		str += ("\n["+p+"] => "+typeof(theObj)+"\n");
		str += ("\n");
		str = print_r(theObj[p], str);
		str += ("\n"); 
	      } else {
		str += ("\n["+p+"] => "+theObj[p]+"\n");
	      }
	    }
	    str += ("\n");
	    return str;
	  }
    },
    
    console: 
	{
	    create: function()
	    {
		var div = document.createElement('div');
		var log = document.createElement('div');
		jQuery(log).attr('id', 'log');
		jQuery(log).attr('style', 'background-color: black;color: white;height: 250px;overflow-y: scroll;display:none;');
		var clear = document.createElement('span');
		jQuery(clear).attr('onclick', 'utils.console.clear()');
		jQuery(clear).attr('style', 'cursor:pointer');
		jQuery(clear).attr('id', 'console_clear');
		jQuery(clear).html('clear');
		
		var show = document.createElement('span');
		jQuery(show).attr('style', 'cursor:pointer');
		jQuery(show).attr('id', 'console_show');
		jQuery(show).html('trace');
		
		jQuery(show).click(function()
		{
		    jQuery('#log').toggle();
		});
		
		jQuery(div).append(clear);
		jQuery(div).append('&nbsp;&nbsp;');
		jQuery(div).append(show);
		jQuery(div).append(log);
		jQuery('body').append(div)
	    },
	    
	    clear: function()
	    {
		jQuery('#log').html('');
	    },
	    trace: function(data)
	    {
		if(jQuery('#log').length == 0)
		{
		    utils.console.create();
		}
		jQuery('#log').append('<hr>');
		jQuery('#log').append(data);
	    }
	},
	
     form: 
	 {
	     getData: function(formElement)
	     {
		var inputs = {};
		function set_inputs(key, data)
		{
		    if(jQuery(data).attr('name'))
		    {
			if(jQuery(data).attr('type') == 'checkbox')
			{
			    inputs[jQuery(data).attr('name')] = jQuery(data).attr('checked');
			}else
			    inputs[jQuery(data).attr('name')] = jQuery(data).val();
		    }

		}
		jQuery(formElement).find('input').each(set_inputs);
		jQuery(formElement).find('select').each(set_inputs);
		jQuery(formElement).find('textarea').each(set_inputs);
		return inputs;
	     }
	 }	
    
    

}