<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of parser
 *
 * @author Antonio
 * orlac@rambler.ru
 */
$pathLib = phpQueryPath;
require_once ($pathLib.'/phpQuery.php');

class parser {
    //put your code here
    
    protected $loader;
    protected $model;
    protected $urlSite = 'http://weplay.ru/games/<page>/?PAGE_SIZE=40';
    protected $rootSite = 'http://weplay.ru/';
    protected $startPage = 1;
    protected $curPage = 1;
    
    protected $log, $fork;
    
    public function __construct(Loader $loader, Model $model, Log $log, Fork $fork) {
	$this->loader = $loader;
	$this->model = $model;
	$this->log = $log;
	$this->fork = $fork;
	//TODO fix
	$f = fopen( dirname(__FILE__).'/out.txt', 'w');
	fclose($f);
    }
    
    /**
     * start of http://weplay.ru/games/
    */
    public function start()
    {
	$url = str_replace('<page>', $this->startPage, $this->urlSite);
	
	//test
	/*$url = dirname(__FILE__).'/tests/list.html';
	$html = file_get_contents($url);
	*/
	
	$html = $this->loader->get($url);
	$links = $this->parseLinkToItems($html);
	$pages = (array) $this->getPages($html);
	
	// если не последняя страница
	while($this->curPage <=  $pages[ count($pages) - 1 ] + 1 ){
	    
	    while($link = array_shift($links)){
		$parseItem = $this->loadItem($link);
		//print_r($parseItem);
		//записываем в файл
		$this->writeToAny($parseItem);
	    }
	    
	    $this->curPage++;
	    $url = str_replace('<page>', $this->curPage, $this->urlSite);
	    $html = $this->loader->get($url);
	    $links = $this->parseLinkToItems($html);
	    $pages = (array) $this->getPages($html);
	}
    }
    
    protected function writeToAny( array $item){
	$this->model->writeToAny( $item );
			    
	/*$f = fopen( dirname(__FILE__).'/out.txt', 'a' );
	$str = $item['title']."\t";
	$str .= implode( ';', $item['cats'] )."\t";
	$str .= implode( ';', $item['genres'] )."\t";
	$str .= $item['lang'] ."\t";
	$str .= $item['img'] ."\t";
	$str .= $item['short_descr'] ."\t";
	echo $str."\n";*/
	/*fwrite($f, $str);
	fclose($f);*/
    } 
    
    /**
     * get links to items from list
    */
    protected function parseLinkToItems($html){
	preg_match_all('/<h2 class="custom"><a href="(.+?)">(.+?)<\/a><\/h2>/ims', $html, $result);
	array_shift($result);
	$result = (array) array_shift($result);
	//последняя  какой-то /buy 
	array_pop($result);
	foreach($result as $key => $val){
	    $result[$key] = $this->rootSite.$val;
	}
	return $result;
    }
    
    /**
     * parse http://weplay.ru/games/platform/geroi_playstation_move_ps3.html
    */
    public function loadItem($url){
	//$html = file_get_contents($url);
	$html = $this->loader->get($url);
	//echo $html;
	$document = phpQuery::newDocument($html);
	$card = $document->find('div.h-card');
	$card = array_shift($card->elements);
	$card = pq($card);
	//echo $card->html();
	
	//картинка, жанр, категории, язык
	//категории
	$cats = $card->find('ul.breadcrumbs > li > a');
	$cats = $cats->elements;
	$_cats = array();
	while($cat = array_shift($cats)){
	    $cat = pq($cat);
	    $_cats[] = $cat->html();
	}
	//print_r($_cats);
	//жанр
	$genres = $card->find('table.params > tr > td > a');
	$genres = $genres->elements;
	$_genres = array();
	while($genre = array_shift($genres)){
	    $genre = pq($genre);
	    $_genres[] = $genre->html();
	}
	//print_r($_genres);
	
	//language
	$lang = $card->find('table.params > tr > td > span > img');
	$lang = $lang->elements;
	$lang = array_shift($lang);
	$lang = pq($lang);
	$lang = $lang->attr('src');
	preg_match( '/([A-Z]{2,3})\.png/', $lang, $langs );
	/*if(!isset($langs[1])){
	    echo $html;
	    exit;
	    //throw new Exception('noi lang');
	}*/
	$lang = $langs[1];
	//echo $lang."\n";
	
	//картинка
	//$img = $card->find('div.console-box-ps3 > a');
	$img = $card->find('div.zoom-gallery3 > div > a');
	
	$img = $img->elements;
	$img = array_shift($img);
	$img = pq($img);
	$_img = $this->rootSite.$img->attr('href');
	//echo $_img."\n";
	
	//описание
	$short_descr = $document->find('div.short-descr-wrap');
	$short_descr = array_shift($short_descr->elements);
	$short_descr = pq($short_descr);
	$short_descr = $short_descr->html();
	//echo $short_descr;
	
	//title
	$title = $document->find('div.b-card-text > h2.custom');
	$title = array_shift($title->elements);
	$title = pq($title);
	$title = $title->text();
	
	return array('cats' => $_cats,
		     'genres' => $_genres,
		     'lang' => $lang,
		     'img' => $_img,
		     'short_descr' => $short_descr,
		     'title' => $title,
		     'url' => $url);
    }
    
    /**
     * get links to pages from http://weplay.ru/games/<page>/?PAGE_SIZE=40
     * @return array (1, 2, 3 ...)
    */
    protected function getPages($html){
	//echo $html;
	$document = phpQuery::newDocument($html);
	$pager = $document->find('ul.pager > li > a');
	$pages = array();
	while($_page = array_shift($pager->elements)){
	    $_page = pq($_page);
	    if( (int) $_page->text() > 0 ){
		$pages[] = (int)$_page->text();
	    }
	}
	$pages = array_unique($pages);
	return $pages;
    }
}
?>
