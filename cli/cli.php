<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
set_time_limit(0);
define('phpQueryPath', realpath(dirname(__FILE__)).'/../phpquery/phpQuery');
require_once ('loader.php');
require_once ('parser.php');
require_once ('model.php');
require_once ('config.php');
require_once ('log.php');
require_once ('fork.php');


function process()
{
    $connect = mysql_connect(db_host, db_user, db_pass) or die('no connect to db');
    mysql_select_db(db_db, $connect);
    $log = new Log('logs/'.date('Y-m-d-H-i').'.log');
    $fork = new Fork();
    $loader = new Loader($log);
    $model = new Model($connect, $log);
    $model->clear();
    $parser = new parser($loader, $model, $log, $fork);
    $parser->start();
}

if($pid = file_get_contents('pid.pid') && file_exists('pid.pid') )
{
    die('process active'.PHP_EOL);
}


if(PHP_OS == 'WINNT')
{
    process();
}else
{
    $pid = pcntl_fork();
    if ($pid == -1) 
    {        
	die('could not fork'.PHP_EOL);
    } else if ($pid) {
	die('die parent process'.PHP_EOL);
    } else {
	file_put_contents('pid.pid', getmypid());
	process();
	unlink('pid.pid');
    }
    posix_setsid();
}

?>
