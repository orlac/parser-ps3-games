<?php
ini_set('display_errors', 1); 
ini_set('log_errors', 1); 
ini_set('error_log', dirname(__FILE__) . '/error_log.txt'); 
error_reporting(E_ALL);
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
set_time_limit(0);
session_start();
define('phpQueryPath', realpath(dirname(__FILE__)).'/phpquery/phpQuery');
require_once ('loader.php');
require_once ('parser.php');
require_once ('model.php');
require_once ('config.php');
require_once ('log.php');
require_once ('fork.php');


$actions = array('getMarks',
		 'getModels',
		 'getYears',
		 'getModification',
		 'getSizes',
		 'getAddTyres',
		 'getAddDiscs'   );

$result = array();
$result['success'] = true;
$result['data'] = array();
$result['errors'] = array();

//$req = $_GET;
$req = $_POST;
if(!isset($_SESSION['log_file']))
{
    $log_file = 'logs/'.date('Y-m-d-H-i').'.log';
    $mode_file = true;
    $_SESSION['log_file'] = $log_file;
}else
{
    $log_file = $_SESSION['log_file'];
    $mode_file = false;
}

if(!$connect = @mysql_connect(db_host, db_user, db_pass))
{
    $result['success'] = false;
    $result['errors'][] = 'no connect to db';
    echo json_encode($result);exit;
}
        

mysql_select_db(db_db, $connect);
$log = new Log($log_file, $mode_file);
$fork = new Fork();
$loader = new Loader($log);
/*if(!isset($_SESSION['model']))
{
    $model = new Model($connect, $log);
    $_SESSION['model'] = $model;
}else
{
    $model = $_SESSION['model'];
}*/
$dbmodel = new Model($connect, $log);
//$model->clear();
$parser = new parser($loader, $dbmodel, $log, $fork);

//test
/*$t = $parser->getMarks();
$t = array_slice($t, 67);
print_r($t);
exit;*/
//end test
$result = array();
$result['success'] = true;
$result['data'] = array();


sleep(3);
switch ($req['action'])
{
    case 'clear':
	$mark = null;
	if(isset($req['params']['mark']))
	{
	    $mark = $req['params']['mark'];
	}
	$dbmodel->clear($mark);
	echo json_encode($result);exit;
	break;
    case 'getParsedMarks':
	$marks = $parser->getMarks();
        $m = array();
        while($item = array_shift($marks))
        {
            $m[] = $item;
        }
	//$myMarks = $dbmodel->getParsedMarks();
        //$mm = array();
        $mm = $dbmodel->getParsedMarks();
        /*while($item = array_shift($myMarks))
        {
            $mm[] = $item;
        }*/
	//print_r($mm);
	$result['data']['parsed'] = $mm;
	$result['data']['all'] = $m;
	
	echo json_encode($result);exit;
	break;
    case 'getMarks':
	$marks = $parser->getMarks();
        /*$m = array();
        while($item = array_shift($marks))
        {
            $m[] = $item;
        }*/
        //print_r($marks);
        //$marks = array_slice($marks, 70, 1);
	$result['data']['params'] = array('fake' => true);
	$result['data']['marks'] = $marks;//$m;
	echo json_encode($result);exit;
	break;
    
    case 'getModels':
	//$mark = urldecode($req['params']['mark']);
        $mark = $req['params']['mark'];
	//echo urldecode( $mark )."\n";
	$result['data'] = $req;
	
	$id_mark = $dbmodel->addMark($mark);
	$result['data']['params']['id_mark'] = $id_mark;
	
        //$mark = iconv('utf-8', 'cp1251', $mark );
	$models = $parser->getModels($mark);
	$result['data']['models'] = $models;
	
	echo json_encode($result);exit;
	break;
    
    case 'getYears':
	
        $mark = $req['params']['mark'];
	$model = $req['params']['model'];
	$id_mark = $req['params']['id_mark'];
	
	$result['data'] = $req;
	
	$id_model = $dbmodel->addModel($id_mark, $model);
	$result['data']['params']['id_model'] = $id_model;
	
	/*$mark = iconv('utf-8', 'cp1251', $mark );
        $model = iconv('utf-8', 'cp1251', $model );*/
        $years = $parser->getYears($mark, $model);
	$result['data']['years'] = $years;
	
	echo json_encode($result);exit;
	break;
    
    case 'getModification':
	$mark = $req['params']['mark'];
	$model = $req['params']['model'];
	$year = $req['params']['year'];
	$id_model = $req['params']['id_model'];
	
	$result['data'] = $req;
	
	$id_year = $dbmodel->addYear($id_model, $year);
	$result['data']['params']['id_year'] = $id_year;
	
	/*$mark = iconv('utf-8', 'cp1251', $mark );
        $model = iconv('utf-8', 'cp1251', $model );*/
        $mods = $parser->getModification($mark, $model, $year);
	$result['data']['mods'] = $mods;
	
	echo json_encode($result);exit;
	break;
    case 'getSizes':
	$mark = $req['params']['mark'];
	$model = $req['params']['model'];
	$year = $req['params']['year'];
	$mod = $req['params']['mod'];
	$id_year = $req['params']['id_year'];
	
	$result['data'] = $req;
	
	$id_modyfi = $dbmodel->addModyfi($id_year, $mod);
	$result['data']['params']['id_modyfi'] = $id_modyfi;
	
	/*$mark = iconv('utf-8', 'cp1251', $mark );
        $mod = iconv('utf-8', 'cp1251', $mod );*/
        $sizes = $parser->getSizes($mark, $model, $mod, $year);
        $tyres = (array) $sizes['tyre'];
        $disks = (array) $sizes['disk'];
        $parser->addDisks($id_modyfi, $disks);
        $parser->addTyres($id_modyfi, $tyres);
        $result['data']['sizes'] = array();//$sizes;
	
	echo json_encode($result);exit;
	break;
	
    case 'getAddTyres':
	$mark = $req['params']['mark'];
	$model = $req['params']['model'];
	$year = $req['params']['year'];
	$mod = $req['params']['mod'];
	$id_year = $req['params']['id_year'];
	$id_modyfi = $req['params']['id_modyfi'];
	$shtat = $req['params']['shtat'];
	$url = $req['params']['url'];
	
	$result['data'] = $req;
	
	$tyres = $parser->getAddTyres($id_modyfi, $shtat, $url);
	$result['data']['tyres'] = $tyres;
	
	echo json_encode($result);exit;
	break;
	
    case 'getAddDiscs':
	$mark = $req['params']['mark'];
	$model = $req['params']['model'];
	$year = $req['params']['year'];
	$mod = $req['params']['mod'];
	$id_year = $req['params']['id_year'];
	$id_modyfi = $req['params']['id_modyfi'];
	$shtat = $req['params']['shtat'];
	$url = $req['params']['url'];
	
	$result['data'] = $req;
	
	$discs = $parser->getAddDiscs($id_modyfi, $shtat, $url);
	$result['data']['discs'] = $discs;
	
	echo json_encode($result);exit;
	break;
}
    

	



?>
