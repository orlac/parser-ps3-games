<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of model
 *
 * @author Antonio
 * orlac@rambler.ru
 */
class Model {
    //put your code here
    
    private $connect, $log;
    
    public function __construct($connect, Log $log) {
	$this->connect = $connect;
	mysql_query("SET NAMES 'utf8'");
	$this->log = $log;
    }
    
    public function query($sql){
	if( !$r = mysql_query($sql, $this->connect) ){
	    throw new Exception(mysql_error($this->connect));
	}
	return $r;
    }
    
    public function writeToAny( array $item){
	$sql = 'insert into ps3_games (title, cats, genres, lang, img, short_descr, url) ';
	
	$sql .= " values ('".mysql_real_escape_string($item['title'])."',
			    '". implode(';', $item['cats'] ) ."',
			    '". implode(';', $item['genres'] ) ."',
			    '". mysql_real_escape_string( $item['lang'] ) ."',
			    '". mysql_real_escape_string( $item['img'] ) ."',
			    '". mysql_real_escape_string( $item['short_descr'] ) ."',
			    '". mysql_real_escape_string( $item['url'] ) ."')
			    on DUPLICATE key UPDATE id=id";
	$this->query($sql);
    }
    
    
    /**
     * cats
    */
    public function writeCats(){
	$sql = 'select DISTINCT(p.cats)   from ps3_games as p';
	$result = $this->query($sql);
	
	while($source_cats = mysql_fetch_assoc($result)){
	    $cats = explode(';', $source_cats['cats']);
	    $parent_id = 0;
	    while($cat = array_shift($cats)){
		$parent_id = $this->writeCat( $cat, $parent_id );
	    }
	    $sql = "update ps3_games set cat_id = ".(int) $parent_id." where cats = '".$source_cats['cats']."'" ;
	    $this->query($sql);
	}
    }
    
    private function writeCat($title, $parent_id){
	$sql = 'select DISTINCT(c.id)  from cats as c where c.parent_id = '. (int) $parent_id. ' and c.title = \''.$title.'\' ';
	$result = $this->query($sql);
	$cat = mysql_fetch_assoc($result);
	if( isset($cat['id']) ){
	    return (int) $cat['id'];
	}else{
	    $sql = "insert into cats (title, parent_id) values ( '".$title."', ".(int) $parent_id." )";
	    $this->query($sql);
	    return mysql_insert_id();
	}
    }
    
    
    /**
     * tags
    */
    public function writeTags(){
	$sql = 'select DISTINCT(p.genres)   from ps3_games as p';
	$result = $this->query($sql);
	
	while($source_tags = mysql_fetch_assoc($result)){
	    $tags = explode(';', $source_tags['genres']);
	    $tag_ids = array();
	    while($tag = array_shift($tags)){
		$tag_ids[] = $this->writeTag( $tag );
	    }
	    $sql = "update ps3_games set tag_ids = '".implode(',', $tag_ids)."' where genres = '".$source_tags['genres']."'" ;
	    $this->query($sql);
	}
    }
    
    public function writeTag($title){
	$sql = 'select t.id  from tags as t where t.title = \''.$title.'\' ';
	$result = $this->query($sql);
	$tag = mysql_fetch_assoc($result);
	if( isset($tag['id']) ){
	    return (int) $tag['id'];
	}else{
	    $sql = "insert into tags (title) values ( '".$title."' )";
	    $this->query($sql);
	    return mysql_insert_id();
	}
    }
    
    /**
     * games
    */
    public function writeGames(){
	$sql = 'select * from ps3_games as p';
	$result = $this->query($sql);
	
	while($source = mysql_fetch_assoc($result)){
	    
	    $game_id = $this->writeGame( $source );
	    
	    $sql = "update ps3_games set game_id = ".$game_id." where id = ".$source['id']."" ;
	    $this->query($sql);
	}
    }
    private function writeGame($data){
	$sql = "select g.id  from games as g where g.title = '".mysql_escape_string( trim( $data['title'] ) )."' and g.cat_id = ".$data['cat_id']." and g.lang = '".$data['lang']."' ";
	$result = $this->query($sql);
	$game = mysql_fetch_assoc($result);
	if( isset($game['id']) ){
	    return (int) $game['id'];
	}else{
	    $sql = "insert into games (cat_id, title, description, lang, status)
		    values ( ".$data['cat_id'].", '".mysql_escape_string( trim($data['title']) )."', '".mysql_escape_string( trim( $data['short_descr'] ) )."', '".$data['lang']."', 1 )";
	    $this->query($sql);
	    return mysql_insert_id();
	}
    }
    
    /**
     * image
    */
    public function writeImages(){
	$sql = 'select p.img, p.game_id   from ps3_games as p';
	$result = $this->query($sql);
	
	while($source = mysql_fetch_assoc($result)){
	    
	    $img_id = $this->writeImg( $source['img'], $source['game_id'] );
	    
	    $sql = "update ps3_games set image_id = ".$img_id." where genres = '".$source['img']."'" ;
	    $this->query($sql);
	}
    }
    private function writeImg($img_url, $game_id){
	$img = file_get_contents($img_url);
	$file_name = explode('/', $img_url);
	$file_name = array_pop($file_name);
	$params = explode('.', $file_name);
	
	$name = array_shift($params);
	$ext = array_shift($params);
	
	if( !is_dir( dirname(__FILE__).'/upload/'.$game_id ) ){
	    mkdir( dirname(__FILE__).'/upload/'.$game_id );
	    chmod( dirname(__FILE__).'/upload/'.$game_id, '0777' );
	}
	
	$f = fopen( dirname(__FILE__).'/upload/'.$game_id.'/'.$file_name, 'w' );
	fwrite($f, $img);
	fclose($f);
	unset($img);
	
	$sql = 'select i.id  from images as i where i.file = \''.$file_name.'\' and i.game_id = '.(int) $game_id .'';
	$result = $this->query($sql);
	$_img = mysql_fetch_assoc($result);
	
	if( isset($_img['id']) ){
	    return (int) $_img['id'];
	}else{
	    $sql = "insert into images (game_id, file) values ( ". (int) $game_id .",  '".$file_name."' )";
	    $this->query($sql);
	    return mysql_insert_id();
	}
    }
    
    
    /**
     * tags
    */
    public function joinTags(){
	$sql = 'select p.game_id, p.tag_ids from ps3_games as p';
	$result = $this->query($sql);
	
	while($source = mysql_fetch_assoc($result)){
	    
	    $tag_ids = explode(',', $source['tag_ids']);
	    while($tag_id = array_shift($tag_ids)){
		
		$sql = 'insert into game_join_tags (game_id, tag_id) values( '.$source['game_id'].', '.$tag_id.' )';
		$sql .= ' on duplicate key update tag_id = '.$tag_id;
		$this->query($sql);
	    }
	}
    }
}

?>
