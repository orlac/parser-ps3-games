<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="" name="keywords" />
<meta content="" name="discription" />
<title></title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script>
<script src="/js/load.js"></script>
<script src="/js/parser4k.js"></script>
<script src="/js/utils.js"></script>

<script>

var indent = 
    {
	getMarks: '&nbsp;',
	getModels: '&nbsp;&nbsp;',
	getYears: '&nbsp;&nbsp;&nbsp;',
	getModification: '&nbsp;&nbsp;&nbsp;&nbsp;',
	getSizes: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
	getAddTyres: "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
	getAddDiscs: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
    }	
var countQueries = 0

function obj_join(sep, data)
{
    var str = ''
    for(var i in data)
	str += data[i]+sep;
    return str;
}

    jQuery(document).ready(function()
    {
	parser4k.getParsed(function(all, parsed)
	{
	    jQuery('#findMarks').html(all.join(' ; '));
	    jQuery('#parsedMarks').html(parsed.join(' ; '));
	    
	    jQuery('#a_parser_start').click(function()
	    {
		jQuery('#a_parser_start').hide();
		parser4k.start(); 
	    });
	});
	
	jQuery(document).bind(parser4k.event.load, function(event, data)
	{
	    if(data == 'start' || data == 'continue')
	    {
		jQuery('#parsedMarks').append(jQuery('<hr />'));
		jQuery('#parsedMarks').append(data);
		jQuery('#parsedMarks').append(jQuery('<hr />'));
	    }
	    countQueries++;
	    data = data || 'load...';
	    jQuery('#load_log').html(data.action + ' '+countQueries);
	    //var ht = jQuery('#text_log').html()+'\n';
	    //ht += utils.print_r_text(data.params, data.action);
	    
	    //jQuery('#text_log').html(ht);
	    //utils.console.trace(utils.print_r(data.params, data.action));
	})
	
	jQuery(document).bind(parser4k.event.complete, function(event, data)
	{
	    
	    data = data || {};
	    if(typeof data.action != 'undefined')
	    {
		var html = indent[data.action];
		/*switch(data.action)
		{
		    case 'getMarks':
			html += obj_join(' ; ', data.data) ;
		    break;
		    case 'getModels':
			html += obj_join(' ; ', data.data) ;
		    break;
		    case 'getYears':
			html += obj_join(' ; ', data.data) ;
		    break;
		    case 'getModification':
			html += obj_join(' ; ', data.data) ;
		    break;
		    case 'getSizes':
			html += '';
		    break;
		    case 'getAddTyres':
			html += 'tyres: '+ data.data.length;
		    break;
		    case 'getAddDiscs':
			html += 'discs: '+ data.data.length;
		    break;
		}*/
	    }else
	    {
		html = data || 'complete...';
	    }
	    //jQuery('#parser_log').append('<p>'+html+'</p>');
	})
    })    
    
    
</script>

</head>
<body>
    <div >
	<table style="width: 100%">
	    <tr>
		<td>
		    Нашлось
		</td>
		<td id="findMarks" style="color:blue;">
		    
		</td>
	    </tr>
	    <tr>
		<td>
		    Спарсилось
		</td>
		<td id="parsedMarks" style="color:green;">
		    
		</td>
	    </tr>
	</table>
	
    </div>
    <div id="load_log">
	
    </div>
    <div style="width: 250px;margin: 0 auto; text-align: center">
	<a href="#0" id="a_parser_start">start</a>
    </div>
    <textarea style="width: 100%;height: 50px;font-size: 9px;display: none;" id="text_log"></textarea>
    <div id="parser_log" style="font-size: 9px;">
	
    </div>    
</body>
</html>