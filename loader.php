<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$pathLib = phpQueryPath;
require_once ($pathLib.'/phpQuery.php');

class Loader
{
    
    public $proxys = array('202.116.160.89:80', '202.133.59.170:6588', '212.88.118.181:8080', '77.254.126.63:80', '92.47.180.18:3128', '122.226.113.54:808', '211.157.104.100:80', '212.156.58.182:8080', '187.58.200.182:80', '222.88.95.77:8080', '212.36.221.35:80', '76.164.223.77:808', '122.225.68.125:8181', '164.77.196.78:80');
    public $headers = array(
	'User-Agent' => 'Mozilla/5.0 (Windows NT 6.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2',
	'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
	'Accept-Language' => 'ru-ru,ru;q=0.8,en-us;q=0.5,en;q=0.3',
	'Accept-Charset: utf-8;q=0.7,*;q=0.7'
    );
    
    private $contentAfterCaptcha;
    
    private $log;
    public function __construct(Log $log) {
	$this->log = $log;
    }
    
    public function get($url)
    {
	$wait = rand(1, 2);
	$this->log->debug($wait." s.. \n");
	$this->log->debug('get url '. $url." \n");
	//$url = urlencode($url);
	print_r('\t encode '. $url." \n");
	sleep($wait);
	$content = $this->getContent($url);
	return $content;
    }
    
    private function getContent($url)
    {
	$ch = curl_init();
	$this->curlSetup($ch, $url);
	$r = curl_exec($ch);
	//curl_close($ch);
	return $r;
    }
    
    function curlSetup(&$ch, $url, $post = false)
    {
	//@unlink(realpath(dirname(__FILE__)). "/cookie/cookie.txt");
	$p = $this->proxys[ array_rand($this->proxys) ];
	ini_set('safe_mode', 'off');
	curl_setopt($ch, CURLOPT_POST, $post);
	curl_setopt($ch, CURLOPT_HEADER, $this->headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_COOKIEFILE, realpath(dirname(__FILE__)). "/cookie/cookie.txt");
	curl_setopt($ch, CURLOPT_COOKIEJAR, realpath(dirname(__FILE__)). "/cookie/cookie.txt");
	//curl_setopt($ch,CURLOPT_PROXY,$p); 
	curl_setopt($ch, CURLOPT_URL, $url);
    }
}
?>
