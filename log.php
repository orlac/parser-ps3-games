<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of log
 *
 * @author Antonio
 * orlac@rambler.ru
 */
class Log {
    //put your code here
    
    private $file;
    
    /**
     * @param $file : file name
     */
    public function __construct($file, $clear = true) {
	$this->file = $file;
	$mode = ($clear)? 'w' : 'a';
	$f = fopen($file, $mode);
	fclose($f);
	
    }
    
    public function debug($data)
    {
	$f = fopen($this->file, 'a');
	fwrite($f, print_r($data, true));
	fwrite($f, "\n");
	fclose($f);
    }
}

?>
